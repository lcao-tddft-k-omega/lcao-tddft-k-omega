#!/usr/bin/env python
# -*- coding: utf-8 - *-

"""Class for applying a postiori an
orbital dependent Derivative Discontinuity Corrections or
Scissors operators to the optical absorption spectra Im[ε]
via the LCAOTDDFT-k-ω method from the 
LCAOTDDFTq0 class' transitions file"""

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

from os.path import exists

from numpy import arange, zeros, ndarray, dot, diagonal

from argparse import ArgumentParser

from ase.parallel import parprint
from gpaw import GPAW


class APostiori(object):
    """Class for regenerating the optical absorption spectra, Im[ε],
    from outputted transitions file and applying an orbital-dependent
    derivative discontinuity correction Δxc, or scissors operator"""
    def __init__(self,
                 filename,
                 eta=0.1,
                 verbose=False,
                 cuttrans=1e-2,
                 omegamin=0.0,
                 omegamax=10.0,
                 domega=0.001):
        """Class for calculating optical absorption spectra Im[ε]
        applying an a postiori derivative discontinuity correction Δxc
        based on the outputed transitions file from the LCAOTDDFTq0 class
        filename	GPAW LCAO calculator filename
        eta		Lorentzian broadening (0.1 eV)
        verbose		Print verbose output (False)
        cuttrans	Transition cutoff (used to find transition filename"""
        self.calc = GPAW(filename, txt=None)
        self.calc.set_positions(self.calc.get_atoms())
        self.nkpts = len(self.calc.get_ibz_k_points())
        self.nspins = self.calc.get_number_of_spins()
        self.nbands = self.calc.get_number_of_bands()
        self.fraction = zeros([self.nspins, self.nkpts, self.nbands])
        self.nelectrons = self.calc.get_number_of_electrons()
        self.eta = eta
        self.verbose = verbose
        self.cuttrans = cuttrans
        outputfilename = filename.split('.gpw')[0]
        transitionfilename = outputfilename+'_cuttrans'+str(cuttrans)+'.dat'
        self.calculated = False
        self.dirs = ['x', 'y', 'z']
        self.transitions = self.read_transitions(transitionfilename)
        self.energies = None
        self.set_energy_range(omegamin, omegamax, domega)
        self.epsilon = zeros([4,len(self.energies)])
        self.ddc1 = 0.0
        self.ddc2 = 0.0
        self.oindex = -1
        

    def read_transitions(self, transitionfilename):
        """Read in transitions from filename and return as a list
        transitionfilename	Name of file containing transition information"""
        assert exists(transitionfilename)
        transitionsfile = open(transitionfilename, 'r')
        transitions = []
        tline = transitionsfile.readline()
        tdata = transitionsfile.readlines()
        for tline in tdata:
            transitions.append(self.get_transition(tline))
        return transitions

    def get_transition(self, tline):
        """Returns transition information from a transition file line
        as a dictionary with keys:
        'energy':	energy in eV
        'intensity':	peak intensity
        'occ_index':	occupied state index
	'unocc_index':	unoccupied state index
        'spin':		spin channel
        'kpt':		k-point index
        'qdir':		polarization direction: 'x', 'y', or 'z'"""
        data = tline.replace(" ", "").strip().strip('[').replace(']',',').split(',')
        transition = {}
        transition['energy'] = float(data[0])
        transition['intensity'] = float(data[1])
        transition['occ_index'] = int(data[2])
        transition['unocc_index'] = int(data[3])
        transition['spin'] = int(data[4])
        transition['kpt'] = int(data[5])
        transition['qdir'] = data[6]
        self.verboseprint(transition)
        assert transition['occ_index'] < self.nelectrons
        assert transition['unocc_index'] >= self.nelectrons/2
        assert transition['spin'] < self.nspins
        assert transition['kpt'] < self.nkpts
        assert transition['qdir'] in self.dirs
        return transition

    def calculate_epsilon(self, energies=None, ddc1=0, ddc2=0, oindex=0):
        """Calculate Im[ε] applying Δxc1 and Δxc2 before and after oindex
        energies	Numpy array of energies (default None)
        ddc1		First Derivative Discontinuity Correction Δxc1
        ddc2		Second Derivative Discontinuity Correction Δxc2
        oindex		Index of orbital at which to break application
    			of ddc1 & ddc2"""
        # Check if a calculation has to be rerun
        if not self.run_calculation(energies, ddc1, ddc2, oindex):
            return self.epsilon
        energies = self.energies
        transitions = self.transitions
        epsilon = 0*self.epsilon
        eta = self.eta
        fractions = self.get_fractions()
        for trans in transitions:
            intensity = trans['intensity']
            delta_eps = trans['energy']
            i = trans['occ_index']
            j = trans['unocc_index']
            spin = trans['spin']
            kpt = trans['kpt']
            frac = fractions[spin,kpt,j]
            ddc = frac*ddc1 + (1-frac)*ddc2
            delta_eps += ddc
            qindex = self.dirs.index(trans['qdir'])+1
            self.verboseprint('qindex =', qindex)
            epsilon[qindex] += eta**2*intensity/((energies - delta_eps)**2 + eta**2)
            epsilon[qindex] -= eta**2*intensity/((energies + delta_eps)**2 + eta**2)            
        epsilon[0] = epsilon[1:].sum(0)
        self.calculated = True
        self.epsilon = epsilon
        return epsilon

    def write_epsilon(self,
                      filename=None,
                      energies=None,
                      ddc1=0.0,
                      ddc2=0.0,
                      oindex=0):
        """Calculate Im[ε] applying Δxc1 and Δxc2 before and after oindex
        filename	Output file name
        energies	Numpy array of energies (default None)
        ddc1		First Derivative Discontinuity Correction Δxc1
        ddc2		Second Derivative Discontinuity Correction Δxc2
        oindex		Index of orbital at which to break application
    			of ddc1 & ddc2"""
        epsilon = self.calculate_epsilon(energies, ddc1, ddc2, oindex)        
        filename  = filename.split('.gpw')[0]
        filename += '_ddc1_'+str(ddc1)
        filename += '_ddc2_'+str(ddc2)
        filename += '_oindex'+str(oindex)
        filename += '.dat'
        ofile = open(filename, 'w')
        energies = self.energies
        for i, energy in enumerate(energies):
            print(energy, epsilon[0][i], epsilon[1][i], epsilon[2][i], epsilon[3][i], file=ofile)
        ofile.close()

    def get_fractions(self):
        """Get fraction of transition states 
        with weights before oindex.  Returns
        numpy array of shape [nspins, nkpts, nbands]"""
        for spin in range(self.nspins):
            for kpt in range(self.nkpts):
                u_i = self.calc.wfs.kd.where_is(spin,kpt)
                overlap_munu = self.calc.wfs.kpt_u[u_i].S_MM
                coeff_nmu = self.calc.wfs.kpt_u[u_i].C_nM.copy()
                coeff_nun = coeff_nmu.conjugate().transpose()
                coeff_nmu[:,self.oindex:] *= 0
                frac_nn = dot(coeff_nmu, dot(overlap_munu, coeff_nun))
                self.fraction[spin, kpt] = diagonal(frac_nn)
        return self.fraction

    def verboseprint(self, *args, **kwargs):
        """Prints if self.verbose is True."""
        if self.verbose:
            parprint(*args, **kwargs)
    
    def set_energy_range(self, omegamin, omegamax, domega):
        """Set the energy range in eV
        omegamin	minimum energy (0 eV)
        omegamax	maximum energy (10 eV)
        domega		energy spacing (0.001 eV)"""
        self.verboseprint('Setting energy range from',
                          omegamin, 'to', omegamax,
                          'eV in increments of ', domega, 'eV')
        self.energies = arange(omegamin, omegamax, domega)

    def run_calculation(self, energies, ddc1, ddc2, oindex):
        """Returns True if a calculation should be run
        energies	energy range in eV (numpy ndarray)
        ddc1		First Δxc1
        ddc2		Second Δxc2
        oindex		Orbital index"""
        done = self.calculated
        self.verboseprint('ddc1 =', ddc1, 'ddc2 =', ddc2, 'oindex =', oindex)
        assert isinstance(ddc1, float) and ddc1 >= 0
        assert isinstance(ddc2, float) and ddc2 >= 0
        assert isinstance(oindex, int) and oindex >= 0
        if energies is None:
            energies = self.energies
        done = done and (energies == self.energies).all()
        done = done and (ddc1 == self.ddc1)
        done = done and (ddc2 == self.ddc2)
        done = done and (oindex == self.oindex)
        self.energies = energies
        self.ddc1 = ddc1
        self.ddc2 = ddc2
        self.oindex = oindex
        self.calculated = done
        return not done

def read_arguments():
    """Input Argument Parsing"""
    parser = ArgumentParser()
    parser.add_argument('-f', '--filename',
                        help='GPAW input file name', type=str)
    parser.add_argument('-ddc1', 
                        help='1st derivative discontinuity correction Δxc1 (%(default)s)',
                        type=float, default=0.0)
    parser.add_argument('-ddc2', 
                        help='2nd derivative discontinuity correction Δxc2 (%(default)s)',
                        type=float, default=0.0)
    parser.add_argument('-oindex',
                        help='index of orbital separating regions (%(default)s)',
                        type=int, default=0)
    parser.add_argument('-wmin', '--omegamin',
                        help='Minimum frequency in eV for plotting Im[ε] (%(default)s)',
                        type=float, default=0.0)
    parser.add_argument('-wmax', '--omegamax',
                        help='Maximum frequency in eV for plotting Im[ε] (%(default)s)',
                        type=float, default=10.0)
    parser.add_argument('-dw', '--domega',
                        help='Frequency step size in eV for plotting Im[ε] (%(default)s)',
                        type=float, default=0.001)
    parser.add_argument('-eta', '--eta',
                        help='Inverse lifetime in eV for plotting Im[ε] (%(default)s)',
                        type=float, default=0.1)
    parser.add_argument('-v', '--verbose',
                        help='increase output verbosity',
                        action='store_true')
    parser.add_argument('-ct', '--cuttrans',
                        help='transition intensity cutoff (%(default)s)',
                        type=float, default=1e-2)
    pargs = parser.parse_args()
    return pargs

def main():
    """Calculate Optical Absorption Im[ε]"""
    args = read_arguments()
    a_postiori = APostiori(filename=args.filename,
                           eta=args.eta,
                           verbose=args.verbose,
                           cuttrans=args.cuttrans,
                           omegamin=args.omegamin,
                           omegamax=args.omegamax,
                           domega=args.domega)
    a_postiori.write_epsilon(filename=args.filename,
                             ddc1=args.ddc1,
                             ddc2=args.ddc2,
                             oindex=args.oindex)

if __name__ == '__main__':
    main()
